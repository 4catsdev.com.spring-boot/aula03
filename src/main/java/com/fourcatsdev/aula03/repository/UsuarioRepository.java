package com.fourcatsdev.aula03.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula03.orm.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
